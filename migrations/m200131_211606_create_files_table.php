<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%files}}`.
 */
class m200131_211606_create_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%files}}', [

            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'url' => $this->string(),
            'author_id' => $this->integer(),
            'project_id' => $this->integer(),

        ]);

        $this->addForeignKey('FK_files_projects', 'files', 'project_id', 'projects', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%files}}');
    }
}
