<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%excel}}`.
 */
class m200131_211753_create_excel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%excel}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'url' => $this->string(),
            'author_id' => $this->integer(),
            'project_id' => $this->integer(),
        ]);

        $this->addForeignKey('FK_excel_projects', 'excel', 'project_id', 'projects', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%excel}}');
    }
}
