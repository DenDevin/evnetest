<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $organization
 * @property int|null $start
 * @property int|null $end
 * @property string|null $role
 * @property string|null $link
 * @property string|null $skills
 * @property string|null $attachments
 * @property string|null $type
 * @property int|null $active
 * @property int|null $sort
 * @property string|null $updated_at
 * @property string|null $created_at
 *
 * @property Excel[] $excels
 * @property Files[] $files
 * @property Skills[] $skills0
 */
class Projects extends \yii\db\ActiveRecord
{
    public $xml_file;
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['active', 'sort'], 'integer'],
            [['attachments', 'xml_file'], 'file'],
            [['skills'], 'safe'],
            [['updated_at', 'created_at'], 'safe'],

            [['title', 'organization', 'role', 'link', 'type', 'start', 'end'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'organization' => 'Organization',
            'start' => 'Start',
            'end' => 'End',
            'role' => 'Role',
            'link' => 'Link',
            'skills' => 'Skills',
            'attachments' => 'Attachments',
            'type' => 'Type',
            'active' => 'Active',
            'sort' => 'Sort',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }


    public function beforeSave($insert)
    {

        if(!empty($this->start))
        {
            $this->start = Yii::$app->formatter->asTimestamp($this->start);
        }

        if(!empty($this->end))
        {
            $this->end = Yii::$app->formatter->asTimestamp($this->end);
        }


        if(!empty($this->skills))
        {
            $this->skills = Json::encode($this->skills);
        }


        return parent::beforeSave($insert);
    }

    public function getExcels()
    {
        return $this->hasMany(Excel::className(), ['project_id' => 'id']);
    }

    /**
     * Gets query for [[Files]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\FilesQuery
     */
    public function getFiles()
    {
        return $this->hasMany(Files::className(), ['project_id' => 'id']);
    }

    /**
     * Gets query for [[Skills0]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\SkillsQuery
     */
    public function getSkills()
    {
        return $this->hasMany(Skills::className(), ['project_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\ProjectsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProjectsQuery(get_called_class());
    }

    public function saveSkills()
    {


    }
}
