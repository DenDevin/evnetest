<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;
use app\models\Skills;
use yii\helpers\ArrayHelper;
use dosamigos\fileupload\FileUploadUI;
use yii\helpers\Json;

?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'organization')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'start')->widget(
        DatePicker::className(), [


        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy'
        ]
    ]);?>

    <?= $form->field($model, 'end')->widget(
        DatePicker::className(), [
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy'
        ]
    ]);?>


    <?= $form->field($model, 'role')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>


    <?=
     $form->field($model, 'skills[]')->widget(Select2::classname(), [

        'data' => ArrayHelper::map(Skills::find()->all(), 'id', 'title'),

        'language' => 'en',
        'options' => [
                'value' => Json::decode($model->skills, true),
                'placeholder' => 'Select skills ...',
                'multiple' => true,

        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= FileUploadUI::widget([
        'model' => $model,
        'attribute' => 'attachments',
        'options' => ['accept' => 'image/*, .xls, .xlsx, .doc, .docx, .pdf, .txt'],
        'url' => ['site/image'],
        'gallery' => false,
        'clientOptions' => [
            'maxFileSize' => 2000000
        ],
        // ...
        'clientEvents' => [
            'fileuploaddone' => 'function(e, data) {
                            
                                console.log(data);
                            }',
            'fileuploadfail' => 'function(e, data) {
                              
                                console.log(data);
                            }',
        ],
    ]); ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'active')->textInput() ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
