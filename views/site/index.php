<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dosamigos\fileupload\FileUpload;
use kartik\select2\Select2;

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">
<div class="row form-group">
    <div class="col-sm-4 form-group">
        <?= Html::a('Create Projects', ['create'], ['class' => 'btn btn-success']) ?>
    </div>

    <div class="col-sm-4 form-group">
        <?= Html::a('Export to Excel', [''], ['class' => 'btn btn-success', 'id' => 'excel_export']) ?>
    </div>
    <div class="col-sm-4 form-group">
        <?= FileUpload::widget([
            'model' => $searchModel,
            'attribute' => 'xml_file',
            'url' => ['site/upload'], // your url, this is just for demo purposes,
            'options' => ['accept' => 'xml/*'],
            'clientOptions' => [
                'maxFileSize' => 2000000
            ],
            // Also, you can specify jQuery-File-Upload events
            // see: https://github.com/blueimp/jQuery-File-Upload/wiki/Options#processing-callback-options
            'clientEvents' => [
                'fileuploaddone' => 'function(e, data) {
                                 $.pjax.reload({container: "#updateProject"});
                                 alert("Excel file loaded! Projects was updated!");
                                
                            }',
                'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
            ],
        ]); ?>
    </div>
</div>




    <?php Pjax::begin(['id' => 'updateProject']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description:ntext',
            'organization',
            'start',
            //'end',
            //'role',
            //'link',
            //'skills:ntext',
            //'attachments:ntext',
            //'type',
            //'active',
            //'sort',
            //'updated_at',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>




</div>


<?
$script = '
            $(document).ready(function(){
            $("#excel_export").click(function(e)
            {
                e.preventDefault();
                $.ajax({
                url: "/site/export",
                method: "GET",
                success: function(response)
                {
                  console.log(response);
                }
                
                
                });
        
        
            });


         });
        
        ';

     $this->registerJs($script);
?>